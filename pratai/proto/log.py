import uuid
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler

handler = logging.FileHandler('pratai.log')
handler.setLevel(logging.INFO)


uuid1 = uuid.uuid4().hex


class AppFilter(logging.Filter):
    def filter(self, record):
        record.function_id = uuid1
        return True

logger.addFilter(AppFilter())

# create a logging format

formatter = logging.Formatter('%(asctime)s - %(function_id)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger

logger.addHandler(handler)

logger.info('Hello world')

try:
    pass
    #raise Exception('plop')
except Exception as err:
    logger.error(err.message, exc_info=True)
