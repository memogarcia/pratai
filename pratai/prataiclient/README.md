# python-prataiclient

# Installation


### Newton:

  * pip install python-prataiclient>=1.0.0<=2.0.0


## CLI

#### Create a function

    pratai function-create --name --ram -o /path/to/zip


#### List functions

    pratai function-list
    # functions running, types, function location, etc.
    
   
#### Get a function

    pratai function-show <function_id>


#### Get help

    pratai --help
    pratai --help function-create

