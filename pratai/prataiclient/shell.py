from cliff.app import App
from cliff.commandmanager import CommandManager


class PrataiCommandManager(CommandManager):
    """ All commands available for the shell are registered here """
    
    SHELL_COMMANDS = {}
    
    def load_commands(self, namespace):
        for name, command_class in self.SHELL_COMMANDS.items():
            self.add_command(name, command_class)


class PrataiShell(App):
    def __init__(self):
        super(PrataiShell, self).__init__(
            description='Python Pratai Client',
            version='1.0.0',
            deferred_help=True,
            command_manager=PrataiCommandManager(None),
        )
    
    def build_option_parser(self, description, version):
        parser = super(FreezerShell, self).build_option_parser(description, version)
        
        return parser
    
    @property
    def client(self):
        
        opts = {
            'token': self.options.os_token,
            'version': self.options.os_identity_api_version,
            'username': self.options.os_username,
            'password': self.options.os_password,
            'tenant_name': self.options.os_tenant_name,
            'auth_url': self.options.os_auth_url,
            'endpoint': self.options.os_backup_url,
            'project_name': self.options.os_project_name,
            'user_domain_name': self.options.os_user_domain_name,
            'project_domain_name': self.options.os_project_domain_name,
            'verify': True or self.options.os_cacert,
            'cert': self.options.os_cert
        }
        # return Client(**opts)
        return


def main(argv=sys.argv[1:]):
    return PrataiShell().run(argv)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))