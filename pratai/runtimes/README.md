# Pratai runtimes

Runtime environments for the functions, they run in containers.

## server

**This should do 2 and only 2 things:**

  * load function from filesystem
  * expose the function as http endpoint
  

an example can be found at [server.py](python27/server.py)


## deployment

Install the underlaying dependencies in a container such as:

  * os
  * compilers, interpreters
  * language tools
  * download user code
  * deploy it
  * install dependencies
  * expose the RUN comand but don't run the container, that's the job of the scheduler

an example can be found at [Dockerfile](python27/Dockerfile)


## Data structure

Request:

    {"key": "value"}


Response:

    {
        "payload" : {...},
        "logs": [...]
    }


## Logs

each function should send the logs back to the scheduler