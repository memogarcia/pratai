# this is the server that runs the user code in a docker container
# I'm thinking to separate this and use it as a plugin from a different repo
# because when we add new backends like go, nodejs, etc or even different versions of python
# having this decouple will be a good idea

# so basically what it has to do is load the file like cliff does but
# I think I'll need to load the file from the filesystem
# and it raises the question, how to deploy that code.

import os
import redis
import sys

def load_function_from_filesystem(path='/etc/pratai'):
    sys.path.append(path)
    
    from new_module import main
    return main

def load_payload():
    env = os.environ
    payload = env["pratai_payload"]
    return payload


def execute_function():
    f = load_function_from_filesystem()
    payload = load_payload()
    result = f(payload)
    return result	


def push_to_q(text):
    pool = redis.ConnectionPool(host='178.62.199.180', port=6379, db=0)
    r = redis.Redis(connection_pool=pool)

    r.set('result', text)

if __name__ == '__main__':
     result = execute_function()
     push_to_q(result)

	
