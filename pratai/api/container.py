import requests


class Container(object):
    def __init__(self):
        self.driver = 'docker'
        self.driver_endpoint = "178.62.199.180"
        self.function_id = None
    
    def send_payload(self, payload=None, port=None):
        url = "{0}/{1}/function/{2}".format(self.driver_endpoint, port, self.function_id)
        r = requests.post(self.driver_endpoint, data=payload)
        return r.text
    
    def run(self, function_id):
        self.function_id = function_id
        r = requests.post("")

    def build(self):
        pass

    def list(self):
        pass
    
    def kill(self):
        pass
    
    def get(self):
        pass
