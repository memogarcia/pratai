# maybe this should be shared between api and server,
# or maybe not, but the point is to store the logs from both sides
# as well the workflows and status


# barbican is shared as well, so this might be better if is called shared instead of storage?
# think about this in a relational way, because elasticsearch is not good for this.

import uuid

from elasticsearch_dsl import DocType, String, Date, Integer, Boolean


class Function(DocType):
    id = String(index='not_analyzed')
    user_id = String(index='not_analyzed')
    tenant_id = String(index='not_analyzed')
    description = String(index='not_analyzed')
    location = String(index='not_analyzed')  # which container
    type = String(index='not_analyzed')
    ram = String(index='not_analyzed')
    times_runed = Integer()
    created_at = Date()
    version = Integer()
    deleted = Boolean()
    is_running = Boolean()
    
    class Meta:
        index = 'pratai'
        
    def save(self, **kwargs):
        self.id = uuid.uuid4().hex
        self.deleted = False
        return super(Function, self).save(**kwargs)


class Log(DocType):
    id = String(index='not_analyzed')
    function_id = Sting(index='not_analyzed')
    user_id = String(index='not_analyzed')
    tenant_id = String(index='not_analyzed')
    function_version = Sting(index='not_analyzed')
    log = Sting(index='not_analyzed')
    created_at = Date()
    
    class Meta:
        index = 'pratai'
    
    def save(self, **kwargs):
        self.id = uuid.uuid4().hex
        return super(Log, self).save(**kwargs)


# this will be added in v2.0
class Pipe(DocType):  # the name could be workflow or flow
    # I think I need to rethink this architecture (lol), 
    # specially function_id and targets,
    # the idea is to have a pipe containing lots
    # of functions, and the relation between functions
    # like a net, each function can have multiple targets
    # but could be nice to start with a linear dependency,
    # one to one dependency.
    # so basically is a linked list
    # in api/v2.0 a multidepency could be added.
    # and think how to avoid circular dependencies
    id = String(index='not_analyzed')
    function_id = String(index='not_analyzed')
    targets = String(index='not_analyzed')
    description = String(index='not_analyzed')
    created_at = Date()
    version = String(index='not_analyzed')
    deleted = Boolean()
    
    class Meta:
        index = 'pratai'
    
    def save(self, **kwargs):
        self.id = uuid.uuid4().hex
        self.deleted = False
        return super(Pipe, self).save(**kwargs)


class Container(DocType):
    id = String(index='not_analyzed')
    location = String(index='not_analyzed')
    server_type = String(index='not_analyzed')
    source = String(index='not_analyzed') # where the user code is
    created_at = Date()
    deleted_at = Date()
    
    class Meta:
        index = 'pratai'
    
    def save(self, **kwargs):
        self.id = uuid.uuid4().hex
        return super(Container, self).save(**kwargs)


if __name__ == '__main__':
    Function.init()
    Log.init()
    Container.init()
    # Pipe.init()