from elasticsearch_dsl.connections import connections
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

from pratai.api.storage.schema import Function


def connection():
    return connections.create_connection(hosts=['localhost'])


def healt():
    return connections.get_connection().cluster.health()


def find_function(function_id):
    """get a function"""
    client = connection()
    s = Search(using=client, index="pratai") \
        .query("match", id=function_id)

    