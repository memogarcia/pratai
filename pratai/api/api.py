from flask import Flask, request, jsonify

from pratai.scheduler.scheduler import Scheduler
from pratai.scheduler.function import Function
from pratai.services_q.manager import run
from pratai.api.container import Container


app = Flask(__name__)


@app.route('/api/v1/functions', methods=['GET'])
def function_list():
    f = Function()
    return jsonify(f.list())


@app.route('/api/v1/functions/<function_id>', methods=["GET"])
def function_show(function_id):
    return ""


@app.route('/api/v1/functions', methods=["POST"])
def function_create():
    # this should return an endpoint
    # with an uuid
    return ""


@app.route('/api/v1/functions/<function_id>', methods=["DELETE"])
def function_delete(function_id):
    return ""


@app.route('/api/v1/functions/<function_id>', methods=["POST"])
def function_execute(function_id):
    # run the container
    # get the endpoint
    # send payload
    # kill container
    container = Container()
    container.run(function_id)
    return ""


if __name__ == "__main__":
    app.run(host="192.168.1.186", port=9698, debug=True)
