# API Gateway for Pratai


## Creating a function

when a function gets created, it enables an endpoint in which the function can be reached.

    # function.py
    
    def local_function(payload):
        # you can create local functions
        # and even use dependencies
        return payload
        
    def main(payload):
        # a main function should always be declared
        # and using a payload as a parameter
        # and should always return a json or a dict
        return local_function(payload)
    
    # pratai cli
    pratai function-create --name music_tag --ram 128 -O /path/to/function.py --type http
    
    # output:
    ########################################################################################
    id: b8d0fbea-97fb-4e0e-9bc0-f2ba072cc7c9
    name: music_tag
    endpoint: https://prata-server.com/api/v1/functions/b8d0fbea-97fb-4e0e-9bc0-f2ba072cc7c9
    public: true
    type: http
    ram: 128
    cpus: 1
    high_availability: false
    ########################################################################################
    
    # execute function
    curl -H "Content-Type: application/json" -X POST -d '{"key":"value"}' https://pratai-server.com/api/v1/functions/b8d0fbea-97fb-4e0e-9bc0-f2ba072cc7c9
    

The api also interacts with the python-prataiclient to push new functions and to execute the functions
