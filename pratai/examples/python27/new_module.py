# a pratai function example
# define a file called new_module.py
# in it, a main function accepting a payload
# should be declared
#
# eg. def main(payload):
#         return payload
#
# a payload is a json coming from a HTTP post
# the return should always be a json or a dict
# you can also use dependencies
# you must provide a requirements.txt
# zip your file using the cli or the ui

def local_function(payload):
    return payload


def main(payload):
    return local_function(payload)
