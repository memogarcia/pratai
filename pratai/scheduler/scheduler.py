from pratai.scheduler.function import Function
from pratai.scheduler.container import Container
from pratai.services_q.manager import run


class Scheduler(object):
    def __init__(self):
        self.endpoint = None

    def execute(self, function_id, payload=None):
        # self._prepare(function_id)
        self.deliver_payload(function_id, self.endpoint, payload)
        # self._clean(function_id)

    def _prepare(self, function_id):
        # self.function_id = function_id
        # self.payload = payload
        #function, function_location = Function().get(function_id)
        self.endpoint = Container().get_endpoint(function_id)

    def _clean(self, function_id):
        Container().kill(function_id)

    def deliver_payload(self, function_id, endpoint, payload=None):
        # should this write to Q or send the request to the driver?
        pass
