class Container(object):
    def __init__(self, driver=None):
        self.driver = driver

    def build(self, name, ram=128, cpus=1):
        pass

    def remove(self, container_id):
        pass

    def list(self):
        pass

    def get(self, container_id):
        pass

    def run(self, container_id):
        print("running")

    def get_endpoint(self, function_location):
        pass

    def kill(self, function_id):
        pass
