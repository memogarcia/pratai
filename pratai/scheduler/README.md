# Pratai Scheduler

The Scheduler is in charge of sending a request to a 
function in a container, so it needs to spwan a new container

when a container finish, it gets killed


## Queue


The sheduler inserts "jobs" to the services_q for post-execution

a job contains:

 * payload of the function
 * metadata for the function
 
 
 example:
 
     {
         "payload" : { ... },
         "metadata: {
             "target": "function_id",
             "next": "function_id",
             "previous": []
         }
     }


### Workflow

when a function gets queued it uses the target to execute the function.

when the function returns a response the scheduler query the db to know who is the next function to be the target.

#### TODO:

how to handle multiple targets?
         

## Events

Rest, subscribers, cron, etc. all of the interfaces to spawn containers are treated as an event. The scheduler is agnostic about where the event comes from.

