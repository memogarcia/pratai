# Pratai Drivers

A driver is a backed that orchestrate a container that contains the custom code


A driver gets load from a config file placed on:

    /etc/pratai/pratai.conf
    

which contains the following:


    driver: dummy


## Endpoints

All drivers must expose the following endpoints:

 * /images GET
 * /containers GET
 * /build POST
 * /run/{container_id} POST
 * /kill/{container_id} POST
 * /pull/{container_name} POST
 * /delete/{container_id} DELETE


# Runtimes

Docker files should be located at /etc/pratai/runtimes/