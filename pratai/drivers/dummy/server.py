from flask import Flask, jsonify, abort

app = Flask(__name__)


@app.route('/api/return_ok', methods=['POST', 'GET'])
def run_ok():
    response = {"payload": "test payload"}
    return jsonify(**response)
    

@app.route('/api/return_fail', methods=['POST', 'GET'])
def run_fail():
    return abort(500)


if __name__ == '__main__':
    app.run(host="16.49.137.58", port=9699, debug=True)
