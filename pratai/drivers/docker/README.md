# Docker driver

## Configure docker

enable docker deamon to be accessible by tcp://127.0.0.1

add this to '/etc/default/docker'

    DOCKER_OPTS='-H tcp://127.0.0.1:4243 -H unix:///var/run/docker.sock'

    sudo service docker restart

## Run a container

flask web server should run on the host where docker is deployed


if the name of the images contains /, needs to be replaced by --- in the request

    http://178.62.199.180:9999/pull/memogarcia---python27


when a new container gets executed it exposes a random port to be accessed to.

from python can be retreived like this:

    resp["NetworkSettings"]["Ports"]["5000/tcp"][0]["HostPort"]


the response should be the output of **docker inspect**


## build a container

a post request should be made with the following payload

    {
        "runtime" : "python27", # language runtime
        "memory": 128, # MB
        "cpu_shares": 1024, # default
        "cpu_cores": "auto", # default
        "tags": [] # metadata about user, tenant, etc
        "location": "https://s3-eu-west-1.amazonaws.com/pratai/python27.zip" # where is the code located
    }