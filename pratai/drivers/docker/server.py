from flask import Flask, jsonify, request
import json

from docker import Client

app = Flask(__name__)

import subprocess


cli = Client(base_url='tcp://127.0.0.1:4243', tls=False, version='1.18')


def create_subprocess(cmd):
    """
    Create a new subprocess in the OS
    :param cmd: command to execute in the subprocess
    :return: the output and errors of the subprocess
    """
    process = subprocess.Popen(cmd,
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               shell=True)
    return process.communicate()



@app.route('/pull/<image_name>', methods=['POST', 'GET'])
def pull_image(image_name):
    name = image_name.replace('---', '/')
    cmd = "docker pull {0}".format(name)
    out, err = create_subprocess(cmd)
    if err:
        return err
    return out


@app.route('/images', methods=['GET'])
def get_images():
    return jsonify(cli.images())


@app.route('/containers', methods=['GET'])
def get_containers():
    return jsonify(cli.containers())

@app.route('/run/<container_id>', methods=['POST'])
def run_container(container_id):
    req = json.loads(request.data)
    name = req.get('name', 'tmp')
    payload = req.get('payload')
    cmd = "docker run -e pratai_payload='{1}' -d {0}".format(name, payload)
    out, err = create_subprocess(cmd)
    if err:
        return err
    return out

@app.route('/kill/<container_id>', methods=['POST'])
def kill_container(container_id):
    cmd = "docker kill {0}".format(container_id)
    out, err = create_subprocess(cmd)
    if err:
        return err
    return out


@app.route('/build', methods=['POST'])
def build_container():
    req = json.loads(request.data)
    runtime = req.get("runtime")
    memory = req.get("memory")
    tags = req.get("tags", [])
    if tags:
        t = ""
        for tag in tags:
            t += " -t {0}".format(tag)
            
    if runtime == "python27":
        out, err = create_subprocess("docker build {0} /etc/pratai/runtimes/python27/".format(t))
        if err:
            return err
        return out
    

if __name__ == '__main__':
    app.run(host='178.62.199.180', port=9998, debug=True)
