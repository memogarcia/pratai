# Pratai

Pratai is a project aim to provide an event driven platform as a service for OpenStack.

# Careful now: Pratai is a work-in-progress project and is under heavy development, things may break.

Pratai aim to provide a way to manage your data workflow without managing any server.

The way to do this is by making your code react to events like a http request or a message in a queue.
Cron and long running executions are also supported by pratai.

### Deploy a HTTP function

    # function.py
    
    def local_function(payload):
        # you can create local functions
        # and even use dependencies
        return payload
        
    def main(payload):
        # a main function should always be declared
        # and using a payload as a parameter
        # and should always return a json or a dict
        return local_function(payload)
    
    # pratai cli
    pratai function-create --name music_tag --ram 128 -O /path/to/function.py --type http
    
    # output:
    ########################################################################################
    id: b8d0fbea-97fb-4e0e-9bc0-f2ba072cc7c9
    name: music_tag
    endpoint: https://prata-server.com/api/v1/functions/b8d0fbea-97fb-4e0e-9bc0-f2ba072cc7c9
    public: true
    type: http
    ram: 128
    cpus: 1
    high_availability: false
    ########################################################################################
    
    # execute function
    curl -H "Content-Type: application/json" -X POST -d '{"key":"value"}' https://pratai-server.com/api/v1/functions/b8d0fbea-97fb-4e0e-9bc0-f2ba072cc7c9


### Language support

Pratai supports the following languages:

  * Python:
    * [documentation](pratai/examples/)
    * [examples](pratai/examples/)
  * Bash:
    * [documentation](pratai/examples/)
    * [examples](pratai/examples/)
  * NodeJS:
    * [documentation](pratai/examples/)
    * [examples](pratai/examples/)
  * Go:
    * [documentation](pratai/examples/)
    * [examples](pratai/examples/)
  * more to come...
  

## Components


** Each component should do as less as possible **

check the [architecture](ARCHITECTURE.md)


### Pratai API gateway

The api takes care of the requests coming via HTTP post.

  * [api](pratai/api/)


### Pratai Agent

The agent takes care of cron, long running and publisher/subscriber functions.

  * [agent](pratai/agent/)


### Pratai Queue

Asynchronous message passing between components in order to achieve a non-blocking platform.

  * [services queue](pratai/services_q/)


### Pratai Client

CLI to interact with pratai

 * [pratai client](pratai/prataiclient/)
 
 
### Pratai Server

Runtime environments for the functions, they run in containers.

 * [pratai server](pratai/server/)
 

### Pratai Scheduler

Whenever a new request gets made the scheduler will retreive a container and deliver the payload via HTTP post to 
the container running a function.


### Pratai Drivers

Drivers connects pratai with backend services for container orchestration and network management.

  * [dummy](pratai/drivers/dummy/)
  * [magnum](pratai/drivers/magnum/)
  * [stackato](pratai/drivers/stackato/)
  * [mesosphere](pratai/drivers/mesosphere/)
  * [docker](pratai/drivers/docker/)
  
## Security
  
Barbican is used to deploy credentials into functions.
  

## Architecture

  * [pratai architecture](ARCHITECTURE.md)


## Community

Join us at #pratai irc channel in freenode


## Getting started

Deploying the development environment

* [Development environment](GETTINGSTARTED.md)


## Building the docs

    cd pratai/doc
    sphinx-build -b html source _build


## Resources

  * [The Reactive Manifesto](http://www.reactivemanifesto.org/)
  * [Compensating Transaction Pattern](https://msdn.microsoft.com/en-us/library/dn589804.aspx)
  * [Even more patterns](https://msdn.microsoft.com/en-us/library/dn600223.aspx)


## FAQ

  * What does Pratai means?
    * Pratai means potato in irish, we started the development in Ireland :)
  * Who should use pratai?
    * People who wants to expose functionality to their users fast, fast, fast.
    * People who want to bring an idea, a PoC, and/or to production a functionality without having to worry about the infrastructure
  * A typical use case
    * A picture gets saved to swift, a pratai function runs to create a thumbnail of the image, the new thumbnail gets stored in swift. no servers involved.  
  * How to pass sensitive data to my function
    * using barbican
  * Can I use someone else's function?
    * yes, a function can be exposed as http endpoint, so as long as that endpoint is reachable, you can execute any function.
  * Can I run github gists here?
    * yes
  * How can I make sure that the functions are tested correctly?
  * Can I integrate this in my CI?
  * Should I refactor my code to use pratai?
    * a pratai function requires a "main" function that accepts a payload, that's the only requirenment so far.

